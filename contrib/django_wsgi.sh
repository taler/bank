#!/bin/bash
# This file is in the public domain
# Launch uwsgi: i.e. connects django to nginx

print_usage () {
  echo Usage ./django_wsgi.sh virtualenv_dir/
  exit
}

PROJ_DIR=""
ENV_DIR=""

#if test -d "$1";
#  then PROJ_DIR=$1
#else echo "Please give a valid project directory"
#     print_usage
#fi

if test -d "$1";
  then ENV_DIR=$1
else echo "Please give a valid virtualenv's directory"
     print_usage
fi

set -e
pids=$(pidof uwsgi || true)
if [[ ! -z "$pids" ]]; then
	kill $pids || true
	sleep 1
	kill -9 $pids || true
fi
echo "Killed previous instance"

source $ENV_DIR/bin/activate
exec uwsgi --master --die-on-term --socket :8000 --module bank_project.wsgi
