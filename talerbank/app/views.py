##
# This file is part of TALER
# (C) 2014, 2015, 2016 INRIA
#
# TALER is free software; you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version. TALER is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
#  You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
#  @author Marcello Stanisci
#  @author Florian Dold

from functools import wraps
import math
import json
import logging
import hashlib
import random
import re
import django.contrib.auth
import django.contrib.auth.views
import django.contrib.auth.forms
from django.db import transaction
from django import forms
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET
from django.views.decorators.http import require_http_methods
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from .models import BankAccount, BankTransaction
from .amount import Amount
from .schemas import validate_data
LOGGER = logging.getLogger(__name__)

##
# Constant value for the biggest number the bank handles.
# This value is just equal to the biggest number that JavaScript
# can handle (because of the wallet).
UINT64_MAX = (2**64) - 1


##
# Exception raised upon failing login.
#
class LoginFailed(Exception):
    hint = "Wrong username/password"
    http_status_code = 401

##
# Exception raised when the public history from
# a ordinary user account is tried to be accessed.
class PrivateAccountException(Exception):
    hint = "The selected account is private"
    http_status_code = 402


##
# Exception raised when some financial operation goes
# beyond the limit threshold.
class DebitLimitException(Exception):
    hint = "Debit too high, operation forbidden."
    http_status_code = 403

##
# Exception raised when some financial operation is
# attempted and both parties are the same account number.
#
class SameAccountException(Exception):
    hint = "Debit and credit account are the same."
    http_status_code = 403

##
# Exception raised when someone tries to reject a
# transaction, but they have no rights to accomplish
# such operation.
class RejectNoRightsException(Exception):
    hint = "You weren't the transaction credit account, " \
           "no rights to reject."
    http_status_code = 403

##
# The authentication for users to log in the bank.
#
class TalerAuthenticationForm(
        django.contrib.auth.forms.AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].widget.attrs["autofocus"] = True
        self.fields["username"].widget.attrs["placeholder"] = "Username"
        self.fields["password"].widget.attrs["placeholder"] = "Password"

##
# Return a empty response.  Used in "/favicon.ico" requests.
#
def ignore(request):
    del request
    return HttpResponse()

##
# Get a flag from the session and clear it.
#
# @param request the HTTP request being served.
# @param name name of the session value that should be retrieved.
# @return the value, if found; otherwise False.
def get_session_flag(request, name):
    if name in request.session:
        ret = request.session[name]
        del request.session[name]
        return ret
    return False


##
# Get a hint from the session and clear it.  A 'hint' is a
# "message" that different parts of the bank can send to each
# other - via the state - communicating what is the state of
# the HTTP session.
#
# @param request the HTTP request being served.
# @param name hint name
# @return the hint (a "null" one if none was found)
def get_session_hint(request, name):
    if name in request.session:
        ret = request.session[name]
        del request.session[name]
        return ret
    # Fail message, success message, hint.
    return False, False, None


##
# Build the list containing all the predefined accounts; the
# list contains, for example, the exchange, the bank itself, and
# all the public accounts (like GNUnet / Tor / FSF / ..)
def predefined_accounts_list():
    account = 2
    ret = []
    for i in settings.TALER_PREDEFINED_ACCOUNTS[1:]:
        ret.append((account, "%s (#%d)" % (i, account)))
        account += 1
    return ret

##
# Thanks to [1], this class provides a dropdown menu that
# can be used within a <select> element, in a <form>.
class InputDatalist(forms.TextInput):

    ##
    # Constructor function.
    #
    # @param self the object itself.
    # @param datalist a list of admitted values.
    # @param name the name of the value that will be sent
    #        along the POST.
    # @param args positional arguments
    # @param kwargs keyword arguments
    # @return the object
    def __init__(self, datalist, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._name = name
        self._datalist = datalist()
        self.attrs.update(
            {"list": "%slist" % name,
             "pattern": "[1-9]+[0-9]*"})

    ##
    # Method that produces the final HTML from the object itself.
    #
    # @param self the object itself
    # @param name the name of the value that will be sent
    #        along the POST
    # @param value value to be sent along the @a name.
    # @param attrs a dict indicating which HTML attribtues should
    #        be defined in the rendered element.
    # @param renderer render engine (left as None, typically); it
    #        is a class that respects the low-level render API from
    #        Django, see [2]
    def render(self, name, value, attrs=None, renderer=None):
        html = super().render(
            name, value, attrs=attrs, renderer=renderer)
        datalist = '<datalist id="%slist">' % self._name
        for dl_value, dl_text in self._datalist:
            datalist += '<option value="%s">%s</option>' \
                % (dl_value, dl_text)
        datalist += "</datalist>"
        return html + datalist


##
# Form for sending wire transfers.  It usually appears in the
# user profile page.
#
class WTForm(forms.Form):
    amount = forms.FloatField(
        min_value=0.1,
        widget=forms.NumberInput(attrs={"class": "currency-input"}))
    receiver = forms.IntegerField(
        min_value=1,
        widget=InputDatalist(predefined_accounts_list, "receiver"))
    subject = forms.CharField()



##
# This method serves the profile page, which is the main
# page where the user interacts with the bank, and also the
# page that the user gets after a successful login.  It has
# to handle the following cases: (1) the user requests the
# profile page after haing POSTed a wire transfer request.
# (2) The user requests the page after having withdrawn coins,
# that means that a wire transfer has been issued to the exchange.
# In this latter case, the method has to notify the wallet about
# the operation outcome.  (3) Ordinary GET case, where the
# straight page should be returned.
# 
# @param request Django-specific HTTP request object.
# @return Django-specific HTTP response object.
@login_required
def profile_page(request):
    if request.method == "POST":
        # WTForm ~ Wire Transfer Form.
        wtf = WTForm(request.POST)
        if wtf.is_valid():
            amount_parts = (settings.TALER_CURRENCY,
                            wtf.cleaned_data.get("amount") + 0.0)
            wire_transfer(
                Amount.parse("%s:%s" % amount_parts),
                BankAccount.objects.get(user=request.user),
                BankAccount.objects.get(account_no=wtf.cleaned_data.get("receiver")),
                wtf.cleaned_data.get("subject"))
            request.session["profile_hint"] = False, True, "Wire transfer successful!"
            return redirect("profile")
    wtf = WTForm()
    fail_message, success_message, hint = get_session_hint(request, "profile_hint")
    context = dict(
        name=request.user.username,
        balance=request.user.bankaccount.amount.stringify(
            settings.TALER_DIGITS, pretty=True),
        sign="-" if request.user.bankaccount.debit else "",
        fail_message=fail_message,
        success_message=success_message,
        hint=hint,
        precision=settings.TALER_DIGITS,
        currency=request.user.bankaccount.amount.currency,
        account_no=request.user.bankaccount.account_no,
        wt_form=wtf,
        history=extract_history(request.user.bankaccount,
                                True),
    )
    if settings.TALER_SUGGESTED_EXCHANGE:
        context["suggested_exchange"] = settings.TALER_SUGGESTED_EXCHANGE

    response = render(request, "profile_page.html", context)
    if "just_withdrawn" in request.session:
        del request.session["just_withdrawn"]
        response["X-Taler-Operation"] = "confirm-reserve"
        response["X-Taler-Reserve-Pub"] = request.session.get(
            "reserve_pub")
        response.status_code = 202
    return response


##
# Helper function that hashes its input.  Usually
# used to hash the response to the math CAPTCHA.
#
# @param ans the plain text answer to hash.
# @return the hashed version of @a ans.
def hash_answer(ans):
    hasher = hashlib.new("sha1")
    hasher.update(settings.SECRET_KEY.encode("utf-8"))
    hasher.update(ans.encode("utf-8"))
    return hasher.hexdigest()


##
# Helper function that makes CAPTCHA's question and
# answer pair.
#
# @return the question and (hashed) answer pair.
def make_question():
    num1 = random.randint(1, 10)
    operand = random.choice(("*", "+", "-"))
    num2 = random.randint(1, 10)
    if operand == "*":
        answer = str(num1 * num2)
    elif operand == "-":
        # ensure result is positive
        num1, num2 = max(num1, num2), min(num1, num2)
        answer = str(num1 - num2)
    else:
        answer = str(num1 + num2)
    question = "{} {} {}".format(num1, operand, num2)
    return question, hash_answer(answer)




##
# This method build the page containing the math CAPTCHA that
# protects coins withdrawal.  It takes all the values from the
# URL and puts them into the state, for further processing after
# a successful answer from the user.
#
# @param request Django-specific HTTP request object
# @return Django-specific HTTP response object
@require_GET
@login_required
def pin_tan_question(request):
    validate_data(request, request.GET.dict())
    user_account = BankAccount.objects.get(user=request.user)
    wire_details = json.loads(request.GET["exchange_wire_details"])
    request.session["exchange_account_number"] = \
        wire_details["test"]["account_number"]
    amount = Amount(request.GET["amount_currency"],
                    int(request.GET["amount_value"]),
                    int(request.GET["amount_fraction"]))
    request.session["amount"] = amount.dump()
    request.session["reserve_pub"] = request.GET["reserve_pub"]
    request.session["sender_wiredetails"] = {
        "type": "test",
        "bank_url": request.build_absolute_uri(reverse("index")),
        "account_number": user_account.account_no}
    fail_message, success_message, hint = get_session_hint(request, "captcha_failed")
    question, hashed_answer = make_question()
    context = dict(
        question=question,
        hashed_answer=hashed_answer,
        amount=amount.stringify(settings.TALER_DIGITS),
        exchange=request.GET["exchange"],
        fail_message=fail_message,
        success_message=success_message,
        hint=hint)
    return render(request, "pin_tan.html", context)




##
# This method serves the user's answer to the math CAPTCHA,
# and reacts accordingly to its correctness.  If correct (wrong),
# it redirects the user to the profile page showing a success
# (failure) message into the informational bar.
@require_POST
@login_required
def pin_tan_verify(request):
    hashed_attempt = hash_answer(request.POST.get("pin_0", ""))
    hashed_solution = request.POST.get("pin_1", "")
    if hashed_attempt != hashed_solution:
        LOGGER.warning("Wrong CAPTCHA answer: %s vs %s",
                       type(hashed_attempt),
                       type(request.POST.get("pin_1")))
        request.session["captcha_failed"] = True, False, "Wrong CAPTCHA answer."
        return redirect(request.POST.get("question_url", "profile"))
    # Check the session is a "pin tan" one
    validate_data(request, request.session)
    amount = Amount(**request.session["amount"])
    exchange_bank_account = BankAccount.objects.get(
        account_no=request.session["exchange_account_number"])
    wire_transfer(amount,
                  BankAccount.objects.get(user=request.user),
                  exchange_bank_account,
                  request.session["reserve_pub"])
    request.session["profile_hint"] = False, True, "Withdrawal successful!"
    request.session["just_withdrawn"] = True
    return redirect("profile")



##
# Class representing the registration form.
class UserReg(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())



##
# This method serves the request for registering a user.
# If successful, it redirects the user to their profile page;
# otherwise it will show again the same form (currently, without
# displaying _any_ error/warning message.)
#
# @param request Django-specific HTTP request object.
# @return Django-specific HTTP response object.
def register(request):
    if request.method != "POST":
        return render(request, "register.html")
    form = UserReg(request.POST)
    if not form.is_valid():
        return render(request, "register.html",
                      {"fail_message": True,
                       "success_message": False,
                       "hint": "Wrong field(s): %s." % ", ".join(form.errors.keys())})
    username = form.cleaned_data["username"]
    password = form.cleaned_data["password"]
    if User.objects.filter(username=username).exists():
        return render(request, "register.html",
                      {"fail_message": True,
                       "success_message": False,
                       "hint": "Username not available."})
    with transaction.atomic():
        user = User.objects.create_user(
            username=username,
            password=password)
        user_account = BankAccount(user=user)
        user_account.save()
    bank_internal_account = BankAccount.objects.get(account_no=1)
    wire_transfer(Amount(settings.TALER_CURRENCY, 100, 0),
                  bank_internal_account,
                  user_account,
                  "Joining bonus")
    request.session["profile_hint"] = False, True, "Registration successful!"
    user = django.contrib.auth.authenticate(
        username=username, password=password)
    django.contrib.auth.login(request, user)
    return redirect("profile")



##
# Logs the user out, redirecting it to the bank's homepage.
#
# @param request Django-specific HTTP request object.
# @return Django-specific HTTP response object.
def logout_view(request):
    django.contrib.auth.logout(request)
    request.session["login_hint"] = False, True, "Logged out!"
    return redirect("index")



##
# Build the history array.
#
# @param account the bank account object whose history is being
#        extracted.
# @param descending currently not used.
# @param delta how many history entries will be contained in the
#        array (will be passed as-is to the internal routine
#        @a query_history).
# @param start any history will be searched starting from this
#        value (which is a row ID), and going to the past or to
#        the future (depending on the user choice).  However, this
#        value itself will not be included in the history.
# @param sign this value ("+"/"-") determines whether the history
#        entries will be younger / older than @a start.
# @return the history array.
def extract_history(account,
                    descending,
                    delta=None,
                    start=UINT64_MAX,
                    sign="+"):
    history = []
    qs = query_history(account, "both", delta, start, sign, True)
    for item in qs:
        if item.credit_account == account:
            counterpart = item.debit_account
            sign = ""
        else:
            counterpart = item.credit_account
            sign = "-"
        entry = dict(
            row_id=item.id,
            cancelled=item.cancelled,
            sign=sign,
            amount=item.amount.stringify(
                settings.TALER_DIGITS, pretty=True),
            counterpart=counterpart.account_no,
            counterpart_username=counterpart.user.username,
            subject=item.subject,
            date=item.date.strftime("%d/%m/%y %H:%M"))
        history.append(entry)
    return history



##
# Serve the page showing histories from publicly visible accounts.
#
# @param request Django-specific HTTP request object.
# @param name name of the public account to show.
# @param page given that public histories are paginated, this
#        value is the page number to display in the response.
# @return Django-specific HTTP response object.
def serve_public_accounts(request, name=None, page=None):
    try:
        page = abs(int(page))
        if page == 0:
            raise Exception
    except Exception:
        page = 1

    if not name:
        name = settings.TALER_PREDEFINED_ACCOUNTS[0]
    user = User.objects.get(username=name)
    if not user.bankaccount.is_public:
        raise PrivateAccountException("Can't display public history for private account")

    num_records = query_history_raw(user.bankaccount,
                                    "both",
                                    start=-1, # makes sign ignored.
                                    sign="+").count()
    DELTA = 30
    # '//' operator is NO floating point.
    num_pages = max(num_records // DELTA, 1)

    public_accounts = BankAccount.objects.filter(is_public=True)

    # Retrieve DELTA records younger than 'start_row' (included).
    history = extract_history(user.bankaccount,
                              True,
                              DELTA * page,
                              -1,
                              "+")[DELTA * (page - 1):(DELTA * page)]

    pages = list(range(1, num_pages + 1))

    context = dict(
        current_page=page,
        back = page - 1 if page > 1 else None,
        forth = page + 1 if page < num_pages else None,
        public_accounts=public_accounts,
        selected_account=dict(
            name=name,
            number=user.bankaccount.account_no,
            history=history,
        ),
        pages=pages
    )
    return render(request, "public_accounts.html", context)

##
# Decorator function that authenticates requests by fetching
# the credentials over the HTTP requests headers.
#
# @param view_func function that will be called after the
#        authentication, and that will usually serve the requested
#        endpoint.
# @return FIXME.
def login_via_headers(view_func):
    def _decorator(request, *args, **kwargs):
        user_account = auth_and_login(request)
        if not user_account:
            LOGGER.error("authentication failed")
            raise LoginFailed("authentication failed")
        return view_func(request, user_account, *args, **kwargs)
    return wraps(view_func)(_decorator)


##
# Helper function that sorts in a descending, or ascending
# manner, the history elements returned by the internal routine
# @a query_history_raw.
#
# @param bank_account the bank account object whose
#        history is being extracted.
# @param direction takes the following three values,
#        * debit: only entries where the querying user has _paid_
#                 will be returned.
#        * credit: only entries where the querying user _got_
#                  paid will be returned.
#        * both: both of the cases above will be returned.
#        * cancel+: only entries where the querying user cancelled
#                   the _receiving_ of money will be returned.
#        * cancel-: only entries where the querying user cancelled
#                   the _paying_ of money will be returned.
# @param delta how many history entries will be contained in the
#        array (will be passed as-is to the internal routine
#        @a query_history).
# @param start any history will be searched starting from this
#        value (which is a row ID), and going to the past or to
#        the future (depending on the user choice).  However, this
#        value itself will not be included in the history.
# @param sign this value ("+"/"-") determines whether the history
#        entries will be younger / older than @a start.
# @param descending if True, then the results will have the
#        youngest entry in the first position.
def query_history(bank_account,
                  direction,
                  delta,
                  start,
                  sign,
                  descending=True):

    direction_switch = {
        "both": (Q(debit_account=bank_account) |
                 Q(credit_account=bank_account)),
        "credit": Q(credit_account=bank_account),
        "debit": Q(debit_account=bank_account),
        "cancel+": (Q(credit_account=bank_account) &
                    Q(cancelled=True)),
        "cancel-": (Q(debit_account=bank_account) &
                    Q(cancelled=True)),
    }

    sign_filter = {
        "+": Q(id__gt=start),
        "-": Q(id__lt=start),
    }

    qs = BankTransaction.objects.filter(
        direction_switch.get(direction),
        sign_filter.get(sign))
    
    order = "-id" if descending else "id"
    return qs.order_by(order)[:delta]


##
# Serve a request of /history.
#
# @param request Django-specific HTTP request.
# @param user_account the account whose history should be gotten.
# @return Django-specific HTTP response object.
@require_GET
@login_via_headers
def serve_history(request, user_account):
    validate_data(request, request.GET.dict())

    # delta (it does exist: enforced by the check above.)
    parsed_delta = re.search(r"([\+-])?([0-9]+)",
                             request.GET.get("delta"))
    # normalize the sign.
    sign = parsed_delta.group(1)
    sign = sign if sign else "+"

    # Ordering.
    ordering = request.GET.get("ordering", "descending")
    start = request.GET.get("start")

    if not start:
        start = 0 if "+" == sign else UINT64_MAX

    qs = query_history(user_account.bankaccount,
                       request.GET.get("direction"),
                       int(parsed_delta.group(2)),
                       int(start),
                       sign,
                       "descending" == ordering)

    history = []
    cancelled = request.GET.get("cancelled", "show")
    for entry in qs:
        counterpart = entry.credit_account.account_no
        sign_ = "-"
        if entry.cancelled and cancelled == "omit":
            continue
        if entry.credit_account.account_no == \
                user_account.bankaccount.account_no:
            counterpart = entry.debit_account.account_no
            sign_ = "+"
        cancel = "cancel" if entry.cancelled else ""
        sign_ = cancel + sign_
        history.append(dict(
            counterpart=counterpart,
            amount=entry.amount.dump(),
            sign=sign_,
            wt_subject=entry.subject,
            row_id=entry.id,
            date="/Date("+str(int(entry.date.timestamp()))+")/"))
    if not history:
        return HttpResponse(status=204)
    return JsonResponse(dict(data=history), status=200)


##
# Helper function that authenticates a user by fetching the
# credentials from the HTTP headers.  Typically called from
# decorators.
#
# @param request Django-specific HTTP request object.
# @return Django-specific "authentication object".
def auth_and_login(request):
    """Return user instance after checking authentication
       credentials, False if errors occur"""

    auth_type = None
    if request.method in ["POST", "PUT"]:
        data = json.loads(request.body.decode("utf-8"))
        auth_type = data["auth"]["type"]
    if request.method == "GET":
        auth_type = request.GET.get("auth")
    if auth_type != "basic":
        LOGGER.error("auth method not supported")
        raise LoginFailed("auth method not supported")

    username = request.META.get("HTTP_X_TALER_BANK_USERNAME")
    password = request.META.get("HTTP_X_TALER_BANK_PASSWORD")
    if not username or not password:
        LOGGER.error("user or password not given")
        raise LoginFailed("missing user/password")
    return django.contrib.auth.authenticate(
        username=username,
        password=password)



##
# Serve a request of /reject (for rejecting wire transfers).
#
# @param request Django-specific HTTP request object.
# @param user_account the account that is going to reject the
#        transfer.  Used to check whether they have this right
#        or not (only accounts which _got_ payed can cancel the
#        transaction.)
@transaction.atomic
@csrf_exempt
@require_http_methods(["PUT", "POST"])
@login_via_headers
def reject(request, user_account):
    data = json.loads(request.body.decode("utf-8"))
    validate_data(request, data)
    trans = BankTransaction.objects.get(id=data["row_id"])
    if trans.credit_account.account_no != \
            user_account.bankaccount.account_no:
        raise RejectNoRightsException()
    trans.cancelled = True
    trans.debit_account.amount.add(trans.amount)
    trans.credit_account.amount.subtract(trans.amount)
    trans.save()
    return HttpResponse(status=204)



##
# Serve a request to make a wire transfer.  Allows fintech
# providers to issues payments in a programmatic way.
#
# @param request Django-specific HTTP request object.
# @param user_account the (authenticated) user issuing this
#        request.
# @return Django-specific HTTP response object.
@csrf_exempt
@require_POST
@login_via_headers
def add_incoming(request, user_account):
    data = json.loads(request.body.decode("utf-8"))
    validate_data(request, data)
    subject = "%s %s" % (data["subject"], data["exchange_url"])
    credit_account = BankAccount.objects.get(
        account_no=data["credit_account"])
    wtrans = wire_transfer(Amount.parse(data["amount"]),
                           user_account.bankaccount,
                           credit_account,
                           subject)
    return JsonResponse(
        {"row_id": wtrans.id,
         "timestamp": "/Date(%s)/" % int(wtrans.date.timestamp())})




##
# Serve a Taler withdrawal request; takes the amount chosen
# by the user, and builds a response to trigger the wallet into
# the withdrawal protocol
#
# @param request Django-specific HTTP request.
# @return Django-specific HTTP response object.
@login_required
@require_POST
def withdraw_nojs(request):

    amount = Amount.parse(
        request.POST.get("kudos_amount", "not-given"))
    user_account = BankAccount.objects.get(user=request.user)
    response = HttpResponse(status=202)
    response["X-Taler-Operation"] = "create-reserve"
    response["X-Taler-Callback-Url"] = reverse("pin-question")
    response["X-Taler-Wt-Types"] = '["test"]'
    response["X-Taler-Amount"] = json.dumps(amount.dump())
    response["X-Taler-Sender-Wire"] = json.dumps(dict(
        type="test",
        bank_url=request.build_absolute_uri(reverse("index")),
        account_number=user_account.account_no
    ))
    if settings.TALER_SUGGESTED_EXCHANGE:
        response["X-Taler-Suggested-Exchange"] = \
            settings.TALER_SUGGESTED_EXCHANGE
    return response


##
# Make a wire transfer between two accounts (internal to the bank)
#
# @param amount how much money the wire transfer is worth.
# @param debit_account the account that gives money.
# @param credit_account the account that receives money.
# @return a @a BankTransaction object.
def wire_transfer(amount, debit_account, credit_account,
                  subject):
    LOGGER.debug("%s => %s, %s, %s" %
                 (debit_account.account_no,
                  credit_account.account_no,
                  amount.stringify(2),
                  subject))
    if debit_account.pk == credit_account.pk:
        LOGGER.error("Debit and credit account are the same!")
        raise SameAccountException()

    transaction_item = BankTransaction(
        amount=amount,
        credit_account=credit_account,
        debit_account=debit_account,
        subject=subject)
    if debit_account.debit:
        debit_account.amount.add(amount)

    elif -1 == Amount.cmp(debit_account.amount, amount):
        debit_account.debit = True
        tmp = Amount(**amount.dump())
        tmp.subtract(debit_account.amount)
        debit_account.amount.set(**tmp.dump())
    else:
        debit_account.amount.subtract(amount)

    if not credit_account.debit:
        credit_account.amount.add(amount)
    elif Amount.cmp(amount, credit_account.amount) == 1:
        credit_account.debit = False
        tmp = Amount(**amount.dump())
        tmp.subtract(credit_account.amount)
        credit_account.amount.set(**tmp.dump())
    else:
        credit_account.amount.subtract(amount)

    # Check here if any account went beyond the allowed
    # debit threshold.

    threshold = Amount.parse(settings.TALER_MAX_DEBT)
    if debit_account.user.username == "Bank":
        threshold = Amount.parse(settings.TALER_MAX_DEBT_BANK)
    if Amount.cmp(debit_account.amount, threshold) == 1 \
            and Amount.cmp(Amount(settings.TALER_CURRENCY),
                           threshold) != 0 \
            and debit_account.debit:
        LOGGER.info("Negative balance '%s' not allowed.\
                    " % json.dumps(debit_account.amount.dump()))
        LOGGER.info("%s's threshold is: '%s'." \
                    % (debit_account.user.username,
                       json.dumps(threshold.dump())))
        raise DebitLimitException()

    with transaction.atomic():
        debit_account.save()
        credit_account.save()
        transaction_item.save()

    return transaction_item

# [1] https://stackoverflow.com/questions/24783275/django-form-with-choices-but-also-with-freetext-option
# [2] https://docs.djangoproject.com/en/2.1/ref/forms/renderers/#low-level-widget-render-api
