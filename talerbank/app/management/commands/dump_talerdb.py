##
# This file is part of TALER
# (C) 2014, 2015, 2106 INRIA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 3, or (at your
# option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
#  You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
# @author Marcello Stanisci
# @brief dump database content in a pretty format.

import sys
import logging
from django.core.management.base import BaseCommand
from django.db.utils import OperationalError, ProgrammingError
from ...models import BankAccount, BankTransaction

LOGGER = logging.getLogger(__name__)


##
# Dump bank accounts that exist in the database.
def dump_accounts():
    try:
        accounts = BankAccount.objects.all()
        if accounts.count() < 1:
            print("No accounts created yet..")
            return
        for acc in accounts:
            print(acc.user.username + \
                  " has account number " + \
                  str(acc.account_no))
    except (OperationalError, ProgrammingError):
        LOGGER.error("Hard database error, does it exist?")
        sys.exit(1)


##
# Dump all the transactions that exist in the database.
def dump_history():
    try:
        history = BankTransaction.objects.all()
        for item in history:
            msg = []
            msg.append("+%s, " % item.credit_account.account_no)
            msg.append("-%s, " % item.debit_account.account_no)
            msg.append(item.amount.stringify(2))
            msg.append(" '" + item.subject + "'")
            print(''.join(msg))
    except (OperationalError, ProgrammingError):
        LOGGER.error("Hard database error, does it exist?")
        sys.exit(1)


##
# Django-specific class that register this CLI utility.
class Command(BaseCommand):

    ##
    # Django-specific callable that gets invoked when the user
    # calls the CLI utility: simply, it calls the two helper
    # functions defined above.
    #
    # @param self this object itself.
    # @param args argument list (as passed by Django)
    # @param options CLI options (also as passed by Django)
    def handle(self, *args, **options):
        dump_accounts()
        dump_history()
