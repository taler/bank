#  This file is part of TALER
#  (C) 2014, 2015, 2016 INRIA
#
#  TALER is free software; you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free Software
#  Foundation; either version 3, or (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
#
#  @author Marcello Stanisci

import logging
from django.test import TestCase
from django.conf import settings
from .amount import Amount, BadFormatAmount

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.WARNING)

class BadMaxDebitOptionTestCase(TestCase):
    def test_badmaxdebitoption(self):
        with self.assertRaises(BadFormatAmount):
            Amount.parse(settings.TALER_MAX_DEBT)
            Amount.parse(settings.TALER_MAX_DEBT_BANK)

# Note, the two following classes are used to check a faulty
# _config_ file, so they are not supposed to have any logic.
class BadDatabaseStringTestCase(TestCase):
    def test_baddbstring(self):
        pass

class NoCurrencyOptionTestCase(TestCase):
    pass
