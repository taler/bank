#  This file is part of TALER
#  (C) 2014, 2015, 2016 INRIA
#
#  TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
#  You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>.
#
#  @author Marcello Stanisci

from django.conf.urls import include, url
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^', include('talerbank.urls')),
    url(r'^$', RedirectView.as_view(pattern_name="profile"),
        name="index"),
    url(r'^favicon\.ico$', views.ignore),
    url(r'^admin/add/incoming$', views.add_incoming,
        name="add-incoming"),
    url(r'^login/$',
        auth_views.LoginView.as_view(
            template_name="login.html",
            authentication_form=views.TalerAuthenticationForm),
        name="login"),
    url(r'^logout/$', views.logout_view, name="logout"),
    url(r'^accounts/register/$', views.register, name="register"),
    url(r'^profile$', views.profile_page, name="profile"),
    url(r'^history$', views.serve_history, name="history"),
    url(r'^reject$', views.reject, name="reject"),
    url(r'^withdraw$', views.withdraw_nojs, name="withdraw-nojs"),
    url(r'^public-accounts$', views.serve_public_accounts,
        name="public-accounts"),
    url(r'^public-accounts/(?P<name>[a-zA-Z0-9]+)$',
        views.serve_public_accounts,
        name="public-accounts"),
    url(r'^public-accounts/(?P<name>[a-zA-Z0-9]+)/(?P<page>[0-9]+)$',
        views.serve_public_accounts,
        name="public-accounts"),
    url(r'^pin/question$', views.pin_tan_question,
        name="pin-question"),
    url(r'^pin/verify$', views.pin_tan_verify, name="pin-verify"),
    ]
