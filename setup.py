# This file is in the public domain.

from setuptools import setup, find_packages

setup(name='talerbank',
      version='0.0',
      description='Taler friendly bank',
      url='git://taler.net/bank',
      author='Marcello Stanisci, Florian Dold',
      author_email='stanisci.m@gmail.com, florian.dold@inria.fr',
      license='GPL',
      packages=find_packages(),
      install_requires=["django>=1.9",
                        "psycopg2",
                        "requests",
                        "uWSGI",
                        "validictory",
                        "mock",
                        "jinja2"
                        ],

      package_data={
        'talerbank.app': [
            'templates/*.html',
            'templates/registration/*.html',
            'static/web-common/*.css',
            'static/web-common/*.html',
            'static/web-common/*.png',
            'static/*.css',
            ]
      },
      scripts=['taler-bank-manage'],
      zip_safe=False)
